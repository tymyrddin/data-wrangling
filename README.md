# Data wrangling

### Learning
- [x] [Diving in](source/notebooks/diving)
- [x] [Data sources](source/notebooks/sources)
- [x] [Some secrets](source/notebooks/secrets)
- [x] [Data gathering](source/notebooks/scraping)
- [x] [RDBMS and SQL](source/notebooks/databases)

...

## Requirements

Anaconda3 with python 3.9 interpreter.

## Installation

### Docker

Build from the Dockerfile:

```bash
$ docker build -t data-wrangling .
```

Run with:

```bash
$ docker run -p 8888:8888 -v $(pwd):/data-wrangling data-wrangling
```

This will give access to jupyter in browser. To enter the cli (in another terminal with the container is running):

```bash
$ docker run -it -v $(pwd):/data-wrangling data-wrangling /bin/bash
```

### Repository

Fork or download, [install anaconda](https://www.anaconda.com/products/individual), and in conda virtual environment for the project, run:

```bash
$ conda env create -f environment.yml
```

After updating packages (and subsequently running the tests):

```bash
$ conda env export > environment.yml --no-builds
```

And the locked dependency list: 

```bash
$ conda env export > environment.lock.yml
```

## Code quality

We automated the code formatting with [black-jupyter](https://anaconda.org/conda-forge/black-jupyter) and PEP8 compliance with [flake8_nb](https://anaconda.org/conda-forge/flake8-nb) processes
by using the [pre-commit framework](https://anaconda.org/conda-forge/pre_commit) and its [hooks](https://anaconda.org/conda-forge/pre-commit-hooks).
It runs a short script before committing. If the script passes, then the commit is made, else, the commit is denied.

Running Black Jupyter on all notebooks from the root of the repo:

```bash
$ black .
```

Running Flake8 NB on all notebooks from the root of the repo:

```bash
$ flake8_nb source/notebooks
```

If you wish to additionally use type annotations and [mypy](https://anaconda.org/anaconda/mypy), install [data-science-types](https://anaconda.org/conda-forge/data-science-types) for libraries like matplotlib, numpy and pandas that do not have type information, and install [nbqa](https://anaconda.org/conda-forge/nbqa).

Run on all notebooks in the root directory of the repo with:

```bash
$ nbqa mypy .
```

Run on a specific `notebook.ipynb` with:

```bash
$ nbqa mypy notebook.ipynb
```

## Testing

* [Pytest-randomly](https://anaconda.org/conda-forge/pytest-randomly) forces the tests to run in a random order. Pytest automatically find the plugin.
* Each test that shows up in the durations report is a good candidate to speed up because it takes an above-average amount of the total testing time.
* To ensure that the unittest suite doesn’t make any real network calls, even if a test accidentally executes the real network call code, we use a [monkeypatch fixture](source/notebooks/tests/conftest.py) to replace values and behaviours, and we use [mock requests](https://anaconda.org/conda-forge/requests-mock).
* During end-to-end testing, we are excluding the databases directory because those notebooks must be executed in a specific order.

To see the [test coverage report](https://anaconda.org/anaconda/pytest-cov):

```bash
$ pytest --cov
```

To get a durations report:

```bash
$ pytest --durations=3
```

To run end-to-end testing:

```bash
$ pytest --nbmake
```

To speed things up with xdist:

```bash
$ pytest --nbmake -n=auto
```

## Security

Check installed dependencies for known security vulnerabilities:

```bash
$ safety check
```

Check Conda packages (python and system):

```bash
$ conda list | jake ddt -c | grep VULNERABLE
```

We also run [Snyk scans](https://app.snyk.io/).

## Project status

Back to Square One.

## Contributing

This project welcomes contributions.

## License

Unlicensed. Or Universal Licensed. Whatever. This is in our playground.
