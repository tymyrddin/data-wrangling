# Introduction

These notebooks serve us to practice some data wrangling techniques, and as snippets to build on. 
We use the [Multidimensional Poverty Index 2021: Unmasking disparities by ethnicity, caste and gender ](https://hdr.undp.org/en/2021-MPI) datasets as a running example.

- [Loading and examining dataset.xlsx](excel_data.ipynb)
- [Missing values](missing.ipynb) 
- [The unique function](unique.ipynb)
- [Setting and resetting an index](indexing.ipynb) 
- [The GroupBy method](groupby.ipynb)
- [Concatenating](concatenating.ipynb)
- [Merging](merging.ipynb)
- [Joining](joining.ipynb)
- [Random sampling](random_sampling.ipynb)
- [Sorting by column values](sorting_columns.ipynb)
- [User-defined functions](user_defined.ipynb)

## Notes

On the dataset:
* Not all indicators were available for all countries, so caution should be used in cross-country comparisons. When an indicator is missing, weights of available indicators are adjusted to total 100 percent. See the [MPI Technical note](http://hdr.undp.org/sites/default/files/mpi2021_technical_notes.pdf) for details.
* There are some missing indicators on nutrition.
* Some values considered child deaths that occurred at any time because the survey did not collect the date of child deaths.
* Indicator on sanitation follows the national classification in which pit latrine with slab is considered unimproved.
* Because of the high proportion of children excluded from nutrition indicators due to measurements not being taken, estimates based on the 2019 Serbia Multiple Indicator Cluster Survey should be interpreted with caution. The unweighted sample size used for the multidimensional poverty calculation is 82.8 percent.

On the Main .xls dataset columns:
* Columns 1 and 2 ("Country" and "Type of Survey"): Refer to the year and the survey whose data were used to calculate the country's Multidimensional Poverty Index value and its components.
* Columns 4-21: HDRO and OPHI calculations based on data on household deprivations in health, education and standard of living from various household surveys listed in columns 1 and 2 using the methodology described in the [MPI Technical note](http://hdr.undp.org/sites/default/files/mpi2021_technical_notes.pdf). Column 7 also uses population data from United Nations Department of Economic and Social Affairs. 2019. World Population Prospects: [The 2019 Revision. Rev 1. New York.](https://esa.un.org/unpd/wpp/). Accessed July, 8 2021.
* Columns 22 and 23: HDRO and OPHI calculations based on the survey presented in columns 1 and 2. Column 23 also uses population data from United Nations Department of Economic and Social Affairs. 2019. World Population Prospects: [The 2019 Revision. Rev 1. New York.](https://esa.un.org/unpd/wpp/). Accessed July 8 2021.
* Column 24: United Nations Department of Economic and Social Affairs. 2019. World Population Prospects: [The 2019 Revision. Rev 1. New York.](https://esa.un.org/unpd/wpp/). Accessed July, 8 2021.
* Column 25: [UNDP classification of developing region.](http://hdr.undp.org/en/content/developing-regions)