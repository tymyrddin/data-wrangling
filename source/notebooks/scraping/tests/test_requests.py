import pytest
from testbook import testbook
import requests
import requests_mock

mock_url = "mock://en.wikipedia.org/wiki/Main_Page"


# Set up a shared notebook context to speed up tests.
@pytest.fixture(scope="module")
def tb():
    with testbook(
        "/scraping/requests.ipynb",
        execute=True,
    ) as tb:
        yield tb


@pytest.fixture(scope="module")
def mock_data():
    session = requests.Session()
    adapter = requests_mock.Adapter()
    session.mount("mock://", adapter)
    adapter.register_uri("GET", mock_url, text="data", status_code=400)
    response = session.get(mock_url)
    session.close()
    return response


def test_url(mock_data):
    assert mock_data.text == "data"


def test_status(mock_data):
    assert mock_data.status_code == 400
